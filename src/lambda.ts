import { Context, Handler } from 'aws-lambda';
import { Server } from 'http';
import { proxy } from 'aws-serverless-express';
import { replaceEventPath } from './common/module/swagger.module';
import { bootstrap } from './main';

let cachedServer: Server;

const prefix = process.env.URL_PRIFIX;

/**
 * serverless.yaml에서 바라보는 서비스A 핸들러 입니다.
 * @param event
 * @param context
 */
export const handler: Handler = async (event: any, context: Context) => {
  event.path = replaceEventPath(event.path, prefix);

  cachedServer = await bootstrap(true);
  return proxy(cachedServer, event, context, 'PROMISE').promise;
};
