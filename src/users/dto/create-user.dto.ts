import { ApiProperty } from '@nestjs/swagger';
import { Prisma } from '@prisma/client';
import { IsEmail, IsString, Matches } from 'class-validator';
import { CommonValidateFunction } from 'src/common/dto/validator/common-validate.function';
import { RoleTypes } from '../../common/constraint/common.const';
import { CommonEntityBuilder } from '../../common/dto/validator/common-entity.builder';
import { BcryptHelper } from '../../common/helper/bcrypt.helper';
import { UserExceptionMessage } from '../exceptions/users.exception.message';

export class CreateUserDto
  implements CommonEntityBuilder<Prisma.UserCreateArgs>
{
  @ApiProperty({
    example: 'mion@gmail.com',
    description: '이메일',
    required: true,
  })
  @IsEmail({ message: '이메일은 문자로 입력해 주십시오.' })
  email: string;

  @ApiProperty({
    example: '미온',
    description: '이름',
    required: true,
  })
  @IsString({ message: '이름은 문자로 입력해 주십시오.' })
  name: string;

  @ApiProperty({
    example: 'password1234!',
    description: '비밀번호',
    required: true,
  })
  @IsString({ message: '비밀번호는 문자로 입력해 주십시오.' })
  @Matches(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/, {
    message: UserExceptionMessage.InvalidPasswordForm(),
  })
  password: string;

  async build(): Promise<Prisma.UserCreateArgs> {
    const model: Prisma.UserCreateArgs = {
      data: {
        email: this.email,
        name: this.name,
        password: await BcryptHelper.hash(this.password),
        role: {
          create: {
            roleId: RoleTypes.USER,
            createdBy: this.email,
            updatedBy: this.email,
          },
        },
        createdBy: this.email,
        updatedBy: this.email,
      },
    };

    return model;
  }

  async validate(validateFunction: CommonValidateFunction<any>): Promise<this> {
    await validateFunction.validate(this);
    return this;
  }
}
