import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { BooleanTypes, User } from '@prisma/client';
import { UserExceptionMessage } from '../../../exceptions/users.exception.message';
import { UsersRepository } from '../../../users.repository';

/**
 * 사용자 공통 유효성 검사
 */
@Injectable()
export class UsersCommonValidator {
  constructor(protected readonly usersRepository: UsersRepository) {}

  /**
   * userId가 존재하는지 검사
   * @param userId
   */
  async validateUserIdExists(userId: number): Promise<User> {
    const savedUser = await this.usersRepository.findFirst({
      where: { id: userId, useYn: BooleanTypes.Y },
    });

    if (!savedUser) {
      throw new NotFoundException(UserExceptionMessage.NotExistsUser(userId));
    }

    return savedUser;
  }

  /**
   * 이메일이 존재하는지 검사
   * @param email
   */
  async validateEmailExists(email: string): Promise<User> {
    const savedUser = await this.usersRepository.findFirst({
      where: { email: email, useYn: BooleanTypes.Y },
    });

    if (!savedUser) {
      throw new BadRequestException(UserExceptionMessage.NotExistsEmail(email));
    }

    return savedUser;
  }
}
