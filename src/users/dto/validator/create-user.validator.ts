import { BadRequestException, Injectable } from '@nestjs/common';
import { BooleanTypes, User } from '@prisma/client';
import { CommonValidateFunction } from '../../../common/dto/validator/common-validate.function';
import { UserExceptionMessage } from '../../exceptions/users.exception.message';
import { CreateUserDto } from '../create-user.dto';
import { UsersCommonValidator } from './common/users-common.validator';

@Injectable()
export class CreateUserValidator
  extends UsersCommonValidator
  implements CommonValidateFunction<CreateUserDto>
{
  async validate(dto: CreateUserDto): Promise<User> {
    const savedUser = await this.usersRepository.findFirst({
      where: { email: dto.email, useYn: BooleanTypes.Y },
    });

    if (savedUser) {
      throw new BadRequestException(
        UserExceptionMessage.DuplicateEmail(dto.email),
      );
    }

    return savedUser;
  }
}
