import { BadRequestException, Injectable } from '@nestjs/common';
import { RoleTypes } from '../../../common/constraint/common.const';
import { CommonValidateFunction } from '../../../common/dto/validator/common-validate.function';
import { UserExceptionMessage } from '../../exceptions/users.exception.message';
import { EditUserDto } from '../edit-user.dto';
import { UsersCommonValidator } from './common/users-common.validator';

@Injectable()
export class EditUserValidator
  extends UsersCommonValidator
  implements CommonValidateFunction<EditUserDto>
{
  async validate(dto: EditUserDto): Promise<void> {
    const savedUser = await super.validateUserIdExists(dto.userId);

    // TODO: role이 USER일 경우, 사용자 ID 일치 여부 확인
    const isUserRole = dto.roles.find((role) => role === RoleTypes.USER);

    const isDifferentUser = savedUser.id !== dto.userId;

    if (isUserRole && isDifferentUser) {
      throw new BadRequestException(
        UserExceptionMessage.NotMatchUserId(savedUser.id, dto.userId),
      );
    }
  }
}
