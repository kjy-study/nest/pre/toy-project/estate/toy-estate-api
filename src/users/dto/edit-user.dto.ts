import { ApiProperty } from '@nestjs/swagger';
import { Prisma } from '@prisma/client';
import { IsOptional, IsString } from 'class-validator';
import { RoleTypes } from '../../common/constraint/common.const';
import { CommonEntityBuilder } from '../../common/dto/validator/common-entity.builder';
import { CommonValidateFunction } from '../../common/dto/validator/common-validate.function';

export class EditUserDto implements CommonEntityBuilder<Prisma.UserUpdateArgs> {
  userId: number;
  updatedBy: string;
  roles: RoleTypes[];

  @ApiProperty({
    example: '수정할 이름',
    description: '이름',
    required: true,
  })
  @IsString({ message: '이름은 문자로 입력해 주십시오.' })
  @IsOptional()
  name: string;

  async build(): Promise<Prisma.UserUpdateArgs> {
    const model: Prisma.UserUpdateArgs = {
      data: {
        name: this.name,
        updatedBy: this.updatedBy,
      },
      where: {
        id: this.userId,
      },
    };
    return model;
  }
  async validate(validateFunction: CommonValidateFunction<any>): Promise<this> {
    await validateFunction.validate(this);
    return this;
  }
}
