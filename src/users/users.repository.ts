import { Injectable } from '@nestjs/common';
import { Prisma, User } from '@prisma/client';
import { PrismaService } from '../common/service/prisma.service';

@Injectable()
export class UsersRepository {
  constructor(private readonly prisma: PrismaService) {}

  /**
   * 사용자 정보 생성
   * @param createUser
   */
  async create(createUser: Prisma.UserCreateArgs) {
    return await this.prisma.user.create(createUser);
  }

  async findFirst(userFindFirstArgs: Prisma.UserFindFirstArgs): Promise<any> {
    return await this.prisma.user.findFirst(userFindFirstArgs);
  }

  /**
   * 사용자 정보 수정
   * @param buildEntity
   */
  async edit(buildEntity: Prisma.UserUpdateArgs) {
    return await this.prisma.user.update(buildEntity);
  }
}
