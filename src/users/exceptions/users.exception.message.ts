export class UserExceptionMessage {
  static NotExistsEmail(email: string): any {
    throw new Error(`Email[${email}]은 존재하지 않는 정보 입니다.`);
  }

  static NotExistsUser(userId: number) {
    return `사용자 ID[${userId}]는 존재하지 않는 정보 입니다.`;
  }

  static DuplicateEmail(email: string) {
    return `사용자 정보 Email[${email}]은 이미 등록되어 있습니다.`;
  }

  static InvalidPasswordForm() {
    return `비밀번호는 최소 8자리 이상, 숫자/특수문자가 각각 1개 이상 조합으로 입력하여 주십시요.`;
  }

  static PasswordMinLength() {
    return '비밀번호는 최소 8자리 이상이여야 합니다.';
  }

  static InvalidPassword() {
    return '비밀번호가 일치하지 않습니다.';
  }

  static NotMatchUserId(savedUserId: number, reqUserId: number) {
    return `등록자 ID(${savedUserId}와 수정자 ID(${reqUserId})가 일치하지 않습니다.`;
  }
}
