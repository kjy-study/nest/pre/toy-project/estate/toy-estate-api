import {
  Body,
  Controller,
  Header,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiExtraModels,
  ApiOperation,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';
import { JwtPayload } from '../auth/auth.service';
import { RoleGuard } from '../auth/guards/role.guard';
import { Public } from '../auth/meta/public.meta';
import { Roles } from '../auth/meta/roles.meta';
import { RoleTypes } from '../common/constraint/common.const';
import { JwtUser } from '../common/decorator/jwt-user.param-decorator';
import { RestParam } from '../common/decorator/rest.param-decorator';
import {
  ApiOperationFormat,
  ApiOperationMethodTypes,
} from '../common/decorator/swagger-operation.decorator';
import {
  ApiCompositeCreateResponse,
  ApiCompositeEditResponse,
} from '../common/decorator/swagger-response.decorator';
import {
  CreateResponseForm,
  EditResponseForm,
  InfoResponseForm,
  ListResponseForm,
  RemoveResponseForm,
} from '../common/response/response.form';
import { CreateUserDto } from './dto/create-user.dto';
import { EditUserDto } from './dto/edit-user.dto';
import { UsersService } from './users.service';

@ApiTags('[사용자] 사용자 정보 관리')
@ApiBearerAuth('access-token')
@Controller({ path: 'users', version: '1' })
@ApiExtraModels(
  ListResponseForm,
  InfoResponseForm,
  CreateResponseForm,
  EditResponseForm,
  RemoveResponseForm,
)
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  @Public()
  @Header('content-type', 'application/json')
  @ApiOperation({
    summary: '사용자 회원가입',
    description: '사용자 정보를 등록 합니다. ',
  })
  @ApiOperationFormat('사용자', ApiOperationMethodTypes.CREATE)
  @ApiCompositeCreateResponse({
    type: CreateResponseForm,
  })
  async create(@Body() dto: CreateUserDto) {
    const savedData = await this.usersService.create({
      dto,
    });
    return new CreateResponseForm(savedData.id);
  }

  @Patch(':userId')
  @Roles(RoleTypes.USER, RoleTypes.ADMIN)
  @UseGuards(RoleGuard)
  @Header('content-type', 'application/json')
  @ApiParam({
    name: 'userId',
    example: '20',
    description: '사용자 ID(PK)',
  })
  @ApiOperationFormat('사용자', ApiOperationMethodTypes.EDIT)
  @ApiCompositeEditResponse({
    type: EditResponseForm,
  })
  async edit(
    @RestParam('userId') userId: number,
    @Body() dto: EditUserDto,
    @JwtUser() user: JwtPayload,
  ) {
    dto.userId = userId;
    dto.updatedBy = user.email;
    dto.roles = user.roles;

    const editedData = await this.usersService.edit({
      dto,
    });

    return new EditResponseForm(editedData.id);
  }
}
