import { Module } from '@nestjs/common';
import { PrismaService } from '../common/service/prisma.service';
import { RolesModule } from '../roles/roles.module';
import { UsersCommonValidator } from './dto/validator/common/users-common.validator';
import { CreateUserValidator } from './dto/validator/create-user.validator';
import { EditUserValidator } from './dto/validator/edit-user.validator';
import { UsersController } from './users.controller';
import { UsersRepository } from './users.repository';
import { UsersService } from './users.service';

@Module({
  imports: [RolesModule],
  controllers: [UsersController],
  providers: [
    UsersService,
    UsersRepository,
    UsersCommonValidator,
    PrismaService,
    CreateUserValidator,
    EditUserValidator,
  ],
  exports: [UsersCommonValidator, UsersRepository],
})
export class UsersModule {}
