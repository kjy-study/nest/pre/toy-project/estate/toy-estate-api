import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import * as dotenv from 'dotenv';
import { PrismaService } from '../common/service/prisma.service';
import { UsersModule } from '../users/users.module';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { LoginValidator } from './dto/validator/login.validator';
import { JwtAuthStrategy } from './strategies/jwt-auth.strategy';
import { LocalAuthStrategy } from './strategies/local-auth.strategy';
dotenv.config();

@Module({
  imports: [
    JwtModule.register({
      secret: `${process.env.JWT_SECRET}`,
      signOptions: {
        algorithm: 'HS512',
        expiresIn: '1h',
      },
    }),
    UsersModule,
  ],
  controllers: [AuthController],
  providers: [
    AuthService,
    LocalAuthStrategy,
    JwtAuthStrategy,
    LoginValidator,
    PrismaService,
  ],
})
export class AuthModule {}
