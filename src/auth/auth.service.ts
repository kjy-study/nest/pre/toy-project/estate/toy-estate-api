import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { BooleanTypes, User } from '@prisma/client';
import { RoleTypes } from '../common/constraint/common.const';
import { PrismaService } from '../common/service/prisma.service';
import { UsersRepository } from '../users/users.repository';
import { CreateTokenForm } from './response/auth-response.form';

export class JwtPayload {
  userId: number;
  email: string;
  name: string;
  roles: RoleTypes[];
}

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly usersRepository: UsersRepository,
    private readonly prisma: PrismaService,
  ) {}

  /**
   * jwt 토큰 생성
   * @param user
   */
  async createToken(user: User): Promise<CreateTokenForm> {
    const savedUser = await this.usersRepository.findFirst({
      where: {
        email: user.email,
        useYn: BooleanTypes.Y,
      },
      include: {
        role: true,
      },
    });

    const jwtPayload = new JwtPayload();
    jwtPayload.userId = savedUser.id;
    jwtPayload.email = savedUser.email;
    jwtPayload.name = savedUser.name;
    jwtPayload.roles = savedUser?.role?.map((roleData) => roleData.roleId);

    return {
      accessToken: this.jwtService.sign(Object.assign({}, jwtPayload)),
      refreshToken: this.jwtService.sign(Object.assign({}, jwtPayload), {
        secret: process.env.JWT_REFESH_SECRET,
        expiresIn: '7d',
      }),
    };
  }
}
