import { Injectable, UnauthorizedException } from '@nestjs/common';
import { User } from '@prisma/client';
import { CommonValidateFunction } from '../../../common/dto/validator/common-validate.function';
import { BcryptHelper } from '../../../common/helper/bcrypt.helper';
import { UsersCommonValidator } from '../../../users/dto/validator/common/users-common.validator';
import { UserExceptionMessage } from '../../../users/exceptions/users.exception.message';
import { LoginDto } from '../login.dto';

/**
 * 로그인 유효성 검사
 */
@Injectable()
export class LoginValidator implements CommonValidateFunction<LoginDto> {
  constructor(private readonly usersCommonValidator: UsersCommonValidator) {}

  async validate(dto: LoginDto): Promise<Omit<User, 'password'>> {
    // email이 존재하는지 검사
    const savedUser = await this.usersCommonValidator.validateEmailExists(
      dto.email,
    );

    // 비밀번호 검사
    if (!(await BcryptHelper.compare(dto.password, savedUser.password))) {
      throw new UnauthorizedException(UserExceptionMessage.InvalidPassword());
    }
    const { password, ...result } = savedUser;

    return result;
  }
}
