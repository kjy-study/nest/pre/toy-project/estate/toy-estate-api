import { ApiProperty } from '@nestjs/swagger';
import { BooleanTypes, Prisma } from '@prisma/client';
import { IsEmail, IsString, Matches } from 'class-validator';
import { CommonEntityBuilder } from '../../common/dto/validator/common-entity.builder';
import { CommonValidateFunction } from '../../common/dto/validator/common-validate.function';
import { UserExceptionMessage } from '../../users/exceptions/users.exception.message';

/**
 * 로그인 요청 DTO
 */
export class LoginDto implements CommonEntityBuilder<Prisma.UserFindFirstArgs> {
  @ApiProperty({
    example: 'mion@gmail.com',
    description: '이메일',
    required: true,
  })
  @IsEmail({ message: '이메일은 문자로 입력해 주십시오.' })
  email: string;

  @ApiProperty({
    example: 'password1234!',
    description: '비밀번호',
    required: true,
  })
  @IsString({ message: '비밀번호는 문자로 입력해 주십시오.' })
  @Matches(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/, {
    message: UserExceptionMessage.InvalidPasswordForm(),
  })
  password: string;

  async build(): Promise<Prisma.UserFindFirstArgs> {
    const model: Prisma.UserFindFirstArgs = {
      where: {
        email: this.email,
        useYn: BooleanTypes.Y,
      },
    };
    return model;
  }

  async validate(validateFunction: CommonValidateFunction<any>): Promise<this> {
    await validateFunction.validate(this);
    return this;
  }
}
