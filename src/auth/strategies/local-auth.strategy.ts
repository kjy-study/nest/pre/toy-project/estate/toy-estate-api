import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { User } from '@prisma/client';
import { Strategy } from 'passport-local';
import { LoginDto } from '../dto/login.dto';
import { LoginValidator } from '../dto/validator/login.validator';

@Injectable()
export class LocalAuthStrategy extends PassportStrategy(Strategy, 'local') {
  constructor(private readonly loginValidator: LoginValidator) {
    super({
      usernameField: 'email',
    });
  }

  async validate(
    email: string,
    password: string,
  ): Promise<Omit<User, 'password'>> {
    const dto = new LoginDto();
    dto.email = email;
    dto.password = password;

    return await this.loginValidator.validate(dto);
  }
}
