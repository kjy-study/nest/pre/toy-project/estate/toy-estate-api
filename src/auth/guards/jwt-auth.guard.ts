import { ExecutionContext, UnauthorizedException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';
import { Observable } from 'rxjs';
import { JwtExceptionMessage } from '../exception/jwt.exception.message';
import { IS_PUBLIC_KEY } from '../meta/public.meta';

export class JwtAuthGuard extends AuthGuard('jwt') {
  constructor(private reflector: Reflector) {
    super();
  }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);
    if (isPublic) {
      return true;
    }

    return super.canActivate(context);
  }

  /**
   * JWT 인증 예외 처리
   * @param err 
   * @param user 
   * @param info 
   * @param context 
   * @param status 
   * @returns 
   */
  handleRequest<TUser = any>(
    err: any,
    user: any,
    info: any,
    context: any,
    status?: any,
  ): TUser {
    if (info && !user) {
      if (info.message === 'jwt expired') {
        throw new UnauthorizedException(JwtExceptionMessage.ExpiredToken(info));
      } else if (info.message === 'jwt malformed') {
        throw new UnauthorizedException(JwtExceptionMessage.MalformedToken());
      } else if (info.message === 'No auth token') {
        throw new UnauthorizedException(JwtExceptionMessage.NotExistsToken());
      } else {
        throw new UnauthorizedException(info);
      }
    }
    return super.handleRequest(err, user, info, context, status);
  }
}
