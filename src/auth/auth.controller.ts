import { Body, Controller, Header, Post, Req, UseGuards } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiCompositeCreateResponse } from '../common/decorator/swagger-response.decorator';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/login.dto';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { Public } from './meta/public.meta';
import { CreateTokenResponseForm } from './response/auth-response.form';

@ApiTags('[공통] 인증')
@Controller({ path: 'auth', version: '1' })
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Public()
  @Post('/token')
  @Header('content-type', 'application/json')
  @UseGuards(LocalAuthGuard)
  @ApiOperation({
    summary: '사용자 인증 및 토큰 발급',
    description: '사용자 인증 및 토큰 발급을 합니다.',
  })
  @ApiCompositeCreateResponse({ type: CreateTokenResponseForm })
  async createToken(@Body() dto: LoginDto, @Req() req) {
    return new CreateTokenResponseForm(
      await this.authService.createToken(req.user),
    );
  }
}
