import { HttpStatus } from '@nestjs/common';
import { ApiProperty } from '@nestjs/swagger';

/**
 * 토큰 등록 처리 반환 결과 FORM
 */
export class CreateTokenResponseForm {
  constructor(params: { accessToken: string; refreshToken: string }) {
    this.accessToken = params.accessToken;
    this.refreshToken = params.refreshToken;
  }

  @ApiProperty({
    example: HttpStatus.CREATED,
    description: '응답 코드',
  })
  statusCode: HttpStatus = HttpStatus.CREATED;

  @ApiProperty({
    example: '정상적으로 토큰 발급 되었습니다.',
    description: '응답 메시지',
  })
  message = '정상적으로 토큰 발급 되었습니다.';

  @ApiProperty({
    example:
      'eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEzMCwiZW1haWwiOiJtaW9uQGdtYWlsLmNvbSIsIm5hbWUiOiLrr7jsmKgiLCJyb2xlcyI6WyJVU0VSIl0sImlhdCI6MTY1Njk0NzMyMCwiZXhwIjoxNjU2OTUwOTIwfQ.HxmzfQcrzFYg_JKyqGhJ5P3wvRgVjwiH19_DUQZvX5SlG32waP90g6k-EUdTXkOqIQ-vSY3jbwlbJlPoUoXsGw',
    description: '발급된 Access Token',
  })
  accessToken: string;

  @ApiProperty({
    example:
      'eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEzMCwiZW1haWwiOiJtaW9uQGdtYWlsLmNvbSIsIm5hbWUiOiLrr7jsmKgiLCJyb2xlcyI6WyJVU0VSIl0sImlhdCI6MTY1Njk0NzMyMCwiZXhwIjoxNjU3NTUyMTIwfQ.tqCw9S72c4E0XCRUm6umd_YqnnnCn09GlHh2E9LQHESrJLK7AsDKGbTcOhPx7OQcY3RczHoBR_LJ-TDg2nSeIQ',
    description: '발급된 Refresh Token',
  })
  refreshToken: string;
}

export class CreateTokenForm {
  accessToken: string;
  refreshToken: string;
}
