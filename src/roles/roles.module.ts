import { Module } from '@nestjs/common';
import { PrismaService } from '../common/service/prisma.service';
import { RolesRepository } from './roles.repository';

@Module({
  providers: [PrismaService, RolesRepository],
  exports: [RolesRepository],
})
export class RolesModule {}
