import { Injectable } from '@nestjs/common';
import { Prisma, Role } from '@prisma/client';
import { PrismaService } from '../common/service/prisma.service';

@Injectable()
export class RolesRepository {
  constructor(private readonly prisma: PrismaService) {}

  async findFirst(roleFindFirstArgs: Prisma.RoleFindFirstArgs): Promise<Role> {
    return await this.prisma.role.findFirst(roleFindFirstArgs);
  }
}
