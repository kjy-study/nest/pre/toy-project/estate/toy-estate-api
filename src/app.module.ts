import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { RoleGuard } from './auth/guards/role.guard';
import { CodesModule } from './codes/codes.module';
import { configuration } from './config/configuration';
import { RolesModule } from './roles/roles.module';
import { UsersModule } from './users/users.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration],
      isGlobal: true,
    }),
    EventEmitterModule.forRoot(),
    AuthModule,
    CodesModule,
    UsersModule,
    RolesModule,
  ],

  controllers: [AppController],
  providers: [AppService, RoleGuard],
})
export class AppModule {}
