export type FindManyResponse = {
  datas: any[];
  count: number;
};

export type FindManyBuilderType<F, C> = {
  findManyArgs: F;
  countManyArgs: C;
};

export type FindUniqueResponse = {
  data: any;
};
