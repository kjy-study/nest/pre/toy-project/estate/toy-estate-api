import * as bcrypt from 'bcrypt';

export class BcryptHelper {
  static async hash(password: string): Promise<string> {
    return await bcrypt.hash(password, Number(process.env.SALT_ROUND));
  }

  static async compare(password: string, hash: string): Promise<boolean> {
    return await bcrypt.compare(password, hash);
  }
}
