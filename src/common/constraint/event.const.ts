export enum UserValidationEventTypes {
  EMAIL_DUPLICATE = 'user.validation.email.duplicate',
  NOT_EXISTS_USER = 'user.validation.not.exists.user',
}

export enum RoleValidationEventTypes {
  NOT_EXISTS = 'role.validation.notExists',
}

export enum CodeValidationEventTypes {
  CODE_DUPLICATE = 'code.validation.code.duplicate',
  NOT_EXISTS_CODE = 'code.validation.not.exists.code',
} 