export enum BooleanTypes {
  Y = 'Y',
  N = 'N',
}

export enum RoleTypes {
  ADMIN = 'ADMIN',
  USER = 'USER',
  SYSTEM = 'SYSTEM',
}
