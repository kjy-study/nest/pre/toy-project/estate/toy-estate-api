/**
 * 리스트 검색결과 페이징 처리가 포함된 반환결과 FORM
 */
import { HttpStatus } from '@nestjs/common';
import { ApiProperty } from '@nestjs/swagger';
import { CommonFilterDto } from '../dto/common-filter.dto';

export class ListResponseForm {
  @ApiProperty({
    example: HttpStatus.OK,
    description: '응답 코드',
  })
  statusCode: HttpStatus = HttpStatus.OK;

  @ApiProperty({
    example: '정상적으로 조회되었습니다.',
    description: '응답 메시지',
  })
  message = '정상적으로 조회되었습니다.';

  @ApiProperty({
    example: 1,
    description: '페이지 인덱스',
  })
  pageIndex: number;

  @ApiProperty({
    example: 5,
    description: '페이지 사이즈',
  })
  pageSize: number;

  @ApiProperty({
    example: 50,
    description: '조회된 데이터 개수',
  })
  total: number;

  @ApiProperty({
    description: '목록 정보',
  })
  items: Array<any>;

  constructor(commonFilterDto: CommonFilterDto, total = 0, items = []) {
    this.pageIndex = commonFilterDto.page;
    this.pageSize = commonFilterDto.size;
    this.total = total;
    this.items = items;
  }
}

/**
 * 데이터  조회 처리 반환 결과 FORM
 */
export class InfoResponseForm {
  @ApiProperty({
    example: HttpStatus.OK,
    description: '응답 코드',
  })
  statusCode: HttpStatus = HttpStatus.OK;

  @ApiProperty({
    example: '정상적으로 조회되었습니다.',
    description: '응답 메시지',
  })
  message = '정상적으로 조회되었습니다.';

  @ApiProperty({
    description: '상세 정보',
  })
  item: object;

  constructor(item: object) {
    this.item = item ?? {};
  }
}

/**
 * 데이터 등록처리 반환 결과 FORM
 */
export class CreateResponseForm {
  @ApiProperty({
    example: HttpStatus.CREATED,
    description: '응답 코드',
  })
  statusCode: HttpStatus = HttpStatus.CREATED;

  @ApiProperty({
    example: '정상적으로 등록되었습니다.',
    description: '응답 메시지',
  })
  message = '정상적으로 등록되었습니다.';
  
  @ApiProperty({
    example: 10,
    description: '신규 등록된 데이터 ID',
  })
  createdId: number;

  constructor(createdId?) {
    this.createdId = createdId;
  }
}

/**
 * 데이터 수정처리 반환 결과 FORM
 */
export class EditResponseForm {
  @ApiProperty({
    example: HttpStatus.OK,
    description: '응답 코드',
  })
  statusCode: HttpStatus = HttpStatus.OK;

  @ApiProperty({
    example: '정상적으로 수정되었습니다.',
    description: '응답 메시지',
  })
  message = '정상적으로 수정되었습니다.';

  @ApiProperty({
    example: 10,
    description: '수정된 데이터 ID',
  })
  editedId: number | string;

  constructor(updateDataId?) {
    this.editedId = updateDataId;
  }
}

/**
 * 데이터 삭제처리 반환 결과 FORM
 */
export class RemoveResponseForm {
  @ApiProperty({
    example: HttpStatus.OK,
    description: '응답 코드',
  })
  statusCode: HttpStatus = HttpStatus.OK;

  @ApiProperty({
    example: '정상적으로 삭제되었습니다.',
    description: '응답 메시지',
  })
  message = '정상적으로 삭제되었습니다.';

  @ApiProperty({
    example: 10,
    description: '삭제된 데이터 ID',
  })
  removedId: number | string;

  constructor(deleteDataId?) {
    this.removedId = deleteDataId;
  }
}
