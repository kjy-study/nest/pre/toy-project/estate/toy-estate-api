import { INestApplication, Injectable, OnModuleInit } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import dayjs from 'dayjs';

@Injectable()
export class PrismaService extends PrismaClient implements OnModuleInit {
  constructor() {
    super();
    // 기본적으로 new Date() 값을 UTC로 저장하기 때문에 9시간을 더해서 저장하도록 미들웨어를 변경합니다.
    this.$use(async (params, next) => {
      if (params.action == 'create' || params.action == 'createMany') {
        params.args.data['createdAt'] = dbNow();
        params.args.data['updatedAt'] = dbNow();
      } else if (params.action == 'update' || params.action == 'updateMany') {
        params.args.data['updatedAt'] = dbNow();
      } else if (params.action == 'delete' || params.action == 'deleteMany') {
        if (params.args?.['updatedAt']) params.args['updatedAt'] = dbNow();
        if (params.args?.['deletedAt']) params.args['deletedAt'] = dbNow();
      }

      const result = await next(params);

      // return prismaTimeMod(result);
      return result;
    });
  }

  /**
   * 선택사항이나 생략하면 Prisma는 데이터베이스에 처음 연결할 때 느립니다.
   */
  async onModuleInit() {
    await this.$connect();
  }

  /**
   * 생략 가능 합니다.
   * @param app
   */
  async enableShutdownHooks(app: INestApplication) {
    this.$on('beforeExit', async () => {
      await app.close();
    });
  }
}

const timezone = 9;

// Subtract 9 hours from all the Date objects recursively
function add9Hours(obj: Record<string, unknown>) {
  if (!obj) return;

  for (const key of Object.keys(obj)) {
    const val = obj[key];

    if (val instanceof Date) {
      obj[key] = dayjs(val).add(timezone, 'hour').toDate();
    } else if (!isPrimitive(val)) {
      add9Hours(val as any);
    }
  }
}

// Subtract 9 hours from all the Date objects recursively
function subtract9Hours(obj: Record<string, unknown>) {
  if (!obj) return;

  for (const key of Object.keys(obj)) {
    const val = obj[key];

    if (val instanceof Date) {
      obj[key] = dayjs(val).subtract(timezone, 'hour').toDate();
    } else if (!isPrimitive(val)) {
      subtract9Hours(val as any);
    }
  }
}

function prismaTimeMod<T>(value: T): T {
  if (value instanceof Date) {
    return dayjs(value).subtract(timezone, 'hour').toDate() as any;
  }

  if (isPrimitive(value)) {
    return value;
  }

  subtract9Hours(value as any);

  return value;
}

function isPrimitive(value: any) {
  return (
    (typeof value !== 'object' && typeof value !== 'function') || value === null
  );
}

// UTC-9 using dayjs
export const dbNow = (): Date => dayjs().add(timezone, 'hour').toDate();
