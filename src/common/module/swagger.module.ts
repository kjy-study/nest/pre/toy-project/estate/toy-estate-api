import { SwaggerModule } from '@nestjs/swagger';

export const swagger_document = (config, prefix, app) => {
  const path = prefix ? `${prefix}/api` : `api`;
  const document = SwaggerModule.createDocument(app, config);
  // const document = SwaggerModule.createDocument(app, config, {
  // include: [AuthModule, PartnersModule],
  // });
  SwaggerModule.setup(path, app, document, {
    swaggerOptions: { defaultModelsExpandDepth: -1 },
  });
};

/**
 * 서버리스 환경에서 Swagger 적용 시 event.path를 수정하기 위한 함수 입니다.
 * @param path
 * @param prefix
 */
export const replaceEventPath = (path: any, prefix?: string) => {
  if (!prefix) {
    if (path === `/api`) {
      path = `/api/`;
    }

    path = path?.includes('swagger-ui') ? `/api${path}` : path;
  } else {
    if (path === `/${prefix}/api`) {
      path = `/${prefix}/api/`;
    }
    if (path?.includes('swagger-ui')) {
      const pathSplit = path.split('/');
      const swaggerPayload = pathSplit[pathSplit.length - 1];

      path = `/${prefix}/api/${swaggerPayload}`;
    }
  }

  return path;
};
