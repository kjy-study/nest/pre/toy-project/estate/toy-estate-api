import {
  BadRequestException,
  createParamDecorator,
  ExecutionContext,
} from '@nestjs/common';

export const RestParam = createParamDecorator(
  (paramKey: string, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const restParam = request?.params[paramKey];

    if (!restParam) {
      new BadRequestException(
        `URL 파라미터에 [${paramKey}] 정보가 존재하지 않습니다.`,
      );
    }

    return restParam;
  },
);
