import { ApiOperation } from '@nestjs/swagger';

/**
 * 등록 API Response 모음
 * @constructor
 */
export function ApiOperationFormat(
  keyword: string,
  method: ApiOperationMethodTypes,
) {
  let methodStr = '';
  switch (method) {
    case ApiOperationMethodTypes.CREATE:
      methodStr = '등록';
      break;
    case ApiOperationMethodTypes.FIND_ALL:
      methodStr = '목록 조회';
      break;
    case ApiOperationMethodTypes.FIND_ONE:
      methodStr = '상세 조회';
      break;
    case ApiOperationMethodTypes.EDIT:
      methodStr = '수정';
      break;
    case ApiOperationMethodTypes.REMOVE:
      methodStr = '삭제';
      break;
  }

  return ApiOperation({
    summary: `${keyword} ${methodStr}`,
    description: `${keyword} 정보를 ${methodStr} 합니다.`,
  });
}

export enum ApiOperationMethodTypes {
  CREATE,
  FIND_ALL,
  FIND_ONE,
  EDIT,
  REMOVE,
}
