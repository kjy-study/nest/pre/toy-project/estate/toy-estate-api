import { applyDecorators } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiResponseOptions
} from '@nestjs/swagger';

/**
 * 등록 API Response 모음
 * @constructor
 */
export function ApiCompositeCreateResponse(options?: ApiResponseOptions) {
  return applyDecorators(
    ApiCreatedResponse({
      description: '정상 등록',
      ...options,
    }),
    ApiBadRequestResponse({
      description: '잘못된 요청',
    }),
    ApiNotFoundResponse({
      description: '존재하지 않는 데이터',
    }),
    ApiConflictResponse({
      description: '중복된 데이터',
    }),
    ApiInternalServerErrorResponse({
      description: '비지니스 로직 오류',
    }),
  );
}

/**
 * 수정 API Response 모음
 * @constructor
 */
export function ApiCompositeEditResponse(options?: ApiResponseOptions) {
  return applyDecorators(
    ApiOkResponse({
      description: '정상 처리',
      ...options,
    }),
    ApiBadRequestResponse({
      description: '잘못된 요청',
    }),
    ApiNotFoundResponse({
      description: '존재하지 않은 데이터 요청',
    }),
    ApiInternalServerErrorResponse({
      description: '비지니스 로직 오류',
    }),
  );
}

/**
 * 삭제 API Response 모음
 * @constructor
 */
export function ApiCompositeRemoveResponse(options?: ApiResponseOptions) {
  return applyDecorators(
    ApiOkResponse({
      description: '정상 처리',
      ...options,
    }),
    ApiBadRequestResponse({
      description: '잘못된 요청',
    }),
    ApiNotFoundResponse({
      description: '존재하지 않은 데이터 요청',
    }),
    ApiInternalServerErrorResponse({
      description: '비지니스 로직 오류',
    }),
  );
}

/**
 * 리스트 조회 API Response 모음
 * @constructor
 */
export function ApiCompositeFindAllResponse(options?: ApiResponseOptions) {
  return applyDecorators(
    ApiOkResponse({
      description: '정상 처리',
      ...options,
    }),
    ApiInternalServerErrorResponse({
      description: '비지니스 로직 오류',
    }),
  );
}

/**
 * 상세정보 API Response 모음
 * @constructor
 */
export function ApiCompositeFindOneResponse(options?: ApiResponseOptions) {
  return applyDecorators(
    ApiOkResponse({
      description: '정상 처리',
      ...options,
    }),
    ApiNotFoundResponse({
      description: '존재하지 않은 데이터 요청',
    }),
    ApiInternalServerErrorResponse({
      description: '비지니스 로직 오류',
    }),
  );
}
