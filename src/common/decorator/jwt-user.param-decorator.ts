import { BadRequestException, createParamDecorator } from '@nestjs/common';

export const JwtUser = createParamDecorator((data, ctx) => {
  const request = ctx.switchToHttp().getRequest();
  const jwtPayload = request?.user;

  if (!jwtPayload) {
    new BadRequestException('JWT 토큰이 존재하지 않습니다.');
  }

  return jwtPayload;
});
