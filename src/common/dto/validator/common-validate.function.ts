export interface CommonValidateFunction<T> {
  validate(dto: T, params?: any): void;
}
