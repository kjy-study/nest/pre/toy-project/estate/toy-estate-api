import { CommonValidateFunction } from './common-validate.function';

export interface CommonEntityBuilder<T> {
  build(params?: any): Promise<T>;
  validate(
    validateFunction: CommonValidateFunction<any>,
    parmas?: any,
  ): Promise<this>;
}
