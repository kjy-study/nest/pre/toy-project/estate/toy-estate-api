/**
 *  리스트 페이징 검색의 공통 DTO
 */
import { ApiProperty } from '@nestjs/swagger';
import { IsNumber } from 'class-validator';
import { Type } from 'class-transformer';

export class CommonFilterDto {
  @ApiProperty({ example: 0, description: '페이지' })
  @IsNumber()
  @Type(() => Number)
  page: number = 0;

  @ApiProperty({ example: 5, description: '한 번에 보여질 데이터 개수' })
  @IsNumber()
  @Type(() => Number)
  size: number = 5;
}
