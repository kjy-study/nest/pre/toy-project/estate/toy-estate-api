export interface CommonRepository {
  create(createEntity: any): Promise<any>;
  findFirst(findFirstArgs: any): Promise<any>;
  findUnique(findUniqueArgs: any): Promise<any>;
  findMany(findManyArgs: any): Promise<any>;
  count(countArgs: any): Promise<any>;
  edit(buildEntity: any): Promise<any>;
  softRemove(buildEntity: any): Promise<any>;
  remove(buildEntity: any): Promise<any>;
}
