import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  HttpStatus,
  Logger,
} from '@nestjs/common';
import { HttpAdapterHost } from '@nestjs/core';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  constructor(private readonly httpAdapterHost: HttpAdapterHost) {}
  catch(exception: any, host: ArgumentsHost): any {
    const { httpAdapter } = this.httpAdapterHost;

    const ctx = host.switchToHttp();

    const httpStatus =
      exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;

    // dto 유효성 에외의 메시지는 exception.response.message에 할당됩니다.
    // http exception 메시지는 exception.messaage에 할당됩니다.
    const message =
      typeof exception.response === 'object' &&
      Array.isArray(exception.response?.message)
        ? exception.response?.message
        : exception.message;

    const responseBody = {
      statusCode: httpStatus,
      message: message,
      timestamp: new Date().toISOString(),
      path: httpAdapter.getRequestUrl(ctx.getRequest()),
    };

    Logger.error(message, exception.stack);

    httpAdapter.reply(ctx.getResponse(), responseBody, httpStatus);
  }
}
