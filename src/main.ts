import { ValidationPipe, VersioningType } from '@nestjs/common';
import { INestApplication } from '@nestjs/common/interfaces/nest-application.interface';
import { ConfigService } from '@nestjs/config';
import { HttpAdapterHost, NestFactory, Reflector } from '@nestjs/core';
import { ExpressAdapter } from '@nestjs/platform-express';
import { DocumentBuilder } from '@nestjs/swagger';
import { createServer } from 'aws-serverless-express';
import { eventContext } from 'aws-serverless-express/middleware';
import { Server } from 'http';
import { AppModule } from './app.module';
import { JwtAuthGuard } from './auth/guards/jwt-auth.guard';
import { RoleGuard } from './auth/guards/role.guard';
import { AllExceptionsFilter } from './common/filter/all-exceptions.filter';
import { swagger_document } from './common/module/swagger.module';
import { PrismaService } from './common/service/prisma.service';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const express = require('express');

const binaryMimeTypes: string[] = [];

let cachedServer: Server;

export async function bootstrap(isServerlessServer?: boolean) {
  if (!cachedServer) {
    const expressApp = express();
    const app = await NestFactory.create(
      AppModule,
      new ExpressAdapter(expressApp),
    );

    const configService = app.get(ConfigService);
    await setGlobalOption(app);

    // swagger
    swagger_document(
      new DocumentBuilder()
        .setTitle(`toy-estate-api`)
        .setDescription(`토이 프로젝트`)
        .setVersion(`0.0.1`)
        .addBearerAuth(
          {
            description: `Bearer 포멧의 토큰을 입력<JWT>`,
            name: 'Authorization',
            bearerFormat: 'Bearer',
            scheme: 'Bearer',
            type: 'http',
            in: 'Header',
          },
          'access-token',
        )
        .build(),
      '',
      app,
    );

    if (isServerlessServer) {
      app.use(eventContext());
      await app.init();
      cachedServer = createServer(expressApp, undefined, binaryMimeTypes);
      return cachedServer;
    } else {
      // 전체 e2e 테스트 실행 시 병렬 처리되면서 'listen EADDRINUSE: address already in use' 에러 발생되어 추가했습니다.
      if (process.env.NODE_ENV !== 'test') {
        await app.listen(configService.get('application.port'));
      }
    }
  }
  return cachedServer;
}

export async function setGlobalOption(app: INestApplication) {
  // URI Versioning
  app.enableVersioning({
    type: VersioningType.URI,
  });

  // pipe
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      transformOptions: {
        // 암묵적 타입 변환
        enableImplicitConversion: true,
      },
      whitelist: true,
      forbidNonWhitelisted: true,
    }),
  );

  // filter
  const httpAdapterHost = app.get(HttpAdapterHost);
  app.useGlobalFilters(new AllExceptionsFilter(httpAdapterHost));

  // guard
  const reflector = app.get(Reflector);
  const jwtAuthGuard = new JwtAuthGuard(reflector);
  const roleGuard = app.get(RoleGuard);
  app.useGlobalGuards(jwtAuthGuard, roleGuard);

  // prisma
  const prismaService = app.get(PrismaService);
  await prismaService.enableShutdownHooks(app);
}

bootstrap();
