export class CodeExceptionMessage {
  static DuplicateCode(codeGroup: string, code: string) {
    return `코드 그룹[${codeGroup}]에 이미 코드[${code}]가 존재합니다.`;
  }

  static NotExistsCode(codeGroup: string, code: string) {
    return `코드 그룹[${codeGroup}]에 코드[${code}]가 존재하지 않습니다.`;
  }

  /* 코드 그룹 */
  static NotExistsCodeGroup(codeGroup: string) {
    return `코드 그룹[${codeGroup}]이 존재하지 않습니다.`;
  }
}
