import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { CommonRepository } from '../common/repository/common.repository';
import { PrismaService } from '../common/service/prisma.service';

@Injectable()
export class CodesRepository implements CommonRepository {
  constructor(private readonly prisma: PrismaService) {}

  async create(codeCreateArgs: Prisma.CodeCreateArgs) {
    return await this.prisma.code.create(codeCreateArgs);
  }

  async findFirst(findFirstArgs: Prisma.CodeFindFirstArgs) {
    return await this.prisma.code.findFirst(findFirstArgs);
  }

  async findMany(findManyArgs: Prisma.CodeFindManyArgs) {
    return await this.prisma.code.findMany(findManyArgs);
  }

  async count(countArgs: Prisma.CodeCountArgs) {
    return await this.prisma.code.count(countArgs);
  }

  async findUnique(codeFindUniqueArgs: Prisma.CodeFindUniqueArgs) {
    return await this.prisma.code.findUnique(codeFindUniqueArgs);
  }

  async edit(codeUpdateArgs: Prisma.CodeUpdateArgs) {
    return await this.prisma.code.update(codeUpdateArgs);
  }

  async softRemove(codeUpdateArgs: Prisma.CodeUpdateArgs) {
    return await this.prisma.code.update(codeUpdateArgs);
  }

  async remove(codeDeleteArgs: Prisma.CodeDeleteArgs) {
    return await this.prisma.code.delete(codeDeleteArgs);
  }
}
