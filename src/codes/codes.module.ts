import { Module } from '@nestjs/common';
import { PrismaService } from '../common/service/prisma.service';
import { CodesController } from './codes.controller';
import { CodesRepository } from './codes.repository';
import { CodesService } from './codes.service';
import { CreateCodeValidator } from './dto/validator/create-code.validator';
import { EditCodeValidator } from './dto/validator/edit-code.validator';
import { RemoveCodeValidator } from './dto/validator/remove-code.validator';

@Module({
  controllers: [CodesController],
  providers: [
    CodesService,
    CodesRepository,
    PrismaService,
    CreateCodeValidator,
    EditCodeValidator,
    RemoveCodeValidator,
  ],
})
export class CodesModule {}
