import { ConflictException, NotFoundException } from '@nestjs/common';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { Test, TestingModule } from '@nestjs/testing';
import { Code } from '@prisma/client';
import { AppModule } from '../app.module';
import { PrismaService } from '../common/service/prisma.service';
import { CodesService } from './codes.service';
import { CreateCodeDto } from './dto/create-code.dto';
import { EditCodeDto } from './dto/edit-code.dto';
import { CodeExceptionMessage } from './exception/codes.exception.message';

jest.setTimeout(60 * 1000);

describe('CodeService', () => {
  let service: CodesService;
  let savedData: Code;
  let prisma: PrismaService;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule, EventEmitterModule.forRoot()],
    }).compile();

    service = module.get<CodesService>(CodesService);
    prisma = module.get<PrismaService>(PrismaService);
  });

  afterAll(async () => {
    if (savedData?.codeGroup) {
      await prisma.code.delete({
        where: {
          codeGroup_code: {
            codeGroup: savedData.codeGroup,
            code: savedData.code,
          },
        },
      });
    }
  });

  describe('등록 테스트', () => {
    const reqCreateCode = {
      code: 'test-code',
      codeGroup: 'service-test',
      codeNm: '테스트 코드',
      createdBy: 'mion@gmail.com',
    };

    it('정상 등록', async () => {
      // 중복 데이터
      const dto = Object.assign(new CreateCodeDto(), reqCreateCode);

      savedData = await service.create({ dto });
    });

    it('코드 중복 예외 발생', async () => {
      // 중복 데이터
      const dto = Object.assign(new CreateCodeDto(), reqCreateCode);

      const result = async () => {
        await service.create({ dto });
      };

      await expect(result()).rejects.toThrowError(
        new ConflictException(
          CodeExceptionMessage.DuplicateCode(dto.codeGroup, dto.code),
        ),
      );
    });
  });

  describe('수정 테스트', () => {
    it('코드 그룹이 없는 데이터 수정 시 오류 반환', async () => {
      const dto = Object.assign(new EditCodeDto(), savedData);
      dto.codeGroup = 'unknown';

      const result = async () => {
        await service.edit({ dto });
      };

      await expect(result()).rejects.toThrowError(
        new NotFoundException(
          CodeExceptionMessage.NotExistsCodeGroup(dto.codeGroup),
        ),
      );
    });

    it('코드가 없는 데이터 수정 시 오류 반환', async () => {
      const dto = Object.assign(new EditCodeDto(), savedData);
      dto.code = 'unknown';

      const result = async () => {
        await service.edit({ dto });
      };

      await expect(result()).rejects.toThrowError(
        new NotFoundException(
          CodeExceptionMessage.NotExistsCode(dto.codeGroup, dto.code),
        ),
      );
    });

    it('정상 수정', async () => {
      const dto = Object.assign(new EditCodeDto(), savedData);
      dto.codeNm = '테스트 코드 수정';

      const asIs = await prisma.code.findUnique({
        where: {
          codeGroup_code: {
            codeGroup: dto.codeGroup,
            code: dto.code,
          },
        },
      });
      await service.edit({ dto });
      const toBe = await prisma.code.findUnique({
        where: {
          codeGroup_code: {
            codeGroup: dto.codeGroup,
            code: dto.code,
          },
        },
      });

      const compareData = Object.assign(asIs);

      // 변하는 값
      compareData.codeNm = dto.codeNm;
      compareData.updatedBy = dto.updatedBy;
      compareData.updatedAt = toBe.updatedAt;

      expect(compareData).toEqual(toBe);
    });
  });
});
