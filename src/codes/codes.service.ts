import { Injectable } from '@nestjs/common';
import { Code } from '@prisma/client';
import { FindManyResponse } from '../common/types/common.type';
import { CodesRepository } from './codes.repository';
import { CreateCodeDto } from './dto/create-code.dto';
import { EditCodeDto } from './dto/edit-code.dto';
import { FindManyCodeDto } from './dto/find-many-code.dto';
import { FindUniqueCodeDto } from './dto/find-unique-code.dto';
import { RemoveCodeDto } from './dto/remove-code.dto';
import { CreateCodeValidator } from './dto/validator/create-code.validator';
import { EditCodeValidator } from './dto/validator/edit-code.validator';
import { RemoveCodeValidator } from './dto/validator/remove-code.validator';

@Injectable()
export class CodesService {
  constructor(
    private readonly codeRepository: CodesRepository,
    private readonly createCodeValidator: CreateCodeValidator,
    private readonly editCodeValidator: EditCodeValidator,
    private readonly removeCodeValidator: RemoveCodeValidator,
  ) {}

  /**
   * 코드 정보 추가
   * @param createCodeDto
   */
  async create(params: { dto: CreateCodeDto }): Promise<Code> {
    await params.dto.validate(this.createCodeValidator);
    const buildEntity = await params.dto.build();
    const savedData = await this.codeRepository.create(buildEntity);

    return savedData;
  }

  /**
   * 코드 정보 목록 조회
   */
  async findMany(params: { dto: FindManyCodeDto }): Promise<FindManyResponse> {
    const { findManyArgs, countManyArgs } = await params.dto.build();
    const savedData = await this.codeRepository.findMany(findManyArgs);
    const count = await this.codeRepository.count(countManyArgs);

    return { datas: savedData, count };
  }

  /**
   * 코드 정보 상세 조회
   * @param code
   */
  async findUnique(params: { dto: FindUniqueCodeDto }) {
    const entityBuilder = await params.dto.build();

    const savedData = await this.codeRepository.findUnique(entityBuilder);
    return savedData;
  }

  /**
   * 코드 정보 수정
   * @param id
   * @param updateCodeDto
   */
  async edit(parmas: { dto: EditCodeDto }) {
    await parmas.dto.validate(this.editCodeValidator);
    const entityBuilder = await parmas.dto.build();
    const editedData = await this.codeRepository.edit(entityBuilder);

    return editedData;
  }

  /**
   * 코드 정보 삭제
   * @param removeCodeDto
   */
  async remove(params: { dto: RemoveCodeDto }) {
    await params.dto.validate(this.removeCodeValidator);
    const entityBuilder = await params.dto.build();

    const removedData = await this.codeRepository.softRemove(entityBuilder);

    return removedData;
  }
}
