import { ApiProperty } from '@nestjs/swagger';
import { BooleanTypes, Code } from '@prisma/client';

export class CodeEntity implements Code {
  @ApiProperty({
    example: 'TEST',
    description: '코드 그룹 (PK)',
    nullable: false,
    required: true,
  })
  codeGroup: string;

  @ApiProperty({
    example: 'CODE_001',
    description: '코드 (PK)',
    nullable: false,
    required: true,
  })
  code: string;

  @ApiProperty({
    example: '코드001',
    description: '코드 이름',
    nullable: false,
    required: true,
  })
  codeNm: string;

  @ApiProperty({
    example: 'mion@gmail.com',
    description: '이메일',
    nullable: false,
    required: true,
  })
  email: string;

  @ApiProperty({
    example: 'mion',
    description: '사용자 이름',
    nullable: true,
    required: true,
  })
  name: string;

  // @ApiProperty({
  //   example: 'password1234!',
  //   description: '사용자 비밀번호',
  //   nullable: false,
  //   required: true,
  // })
  password: string;

  @ApiProperty({
    example: new Date(),
    description: '등록일시',
    enum: BooleanTypes,
    nullable: false,
    required: false,
  })
  createdAt: Date;

  @ApiProperty({
    example: 'mion',
    description: '등록자',
    nullable: false,
    required: true,
  })
  createdBy: string;

  @ApiProperty({
    example: new Date(),
    description: '수정일시',
    nullable: false,
    required: false,
  })
  updatedAt: Date;

  @ApiProperty({
    example: 'mion',
    description: '수정자',
    nullable: false,
    required: true,
  })
  updatedBy: string;

  @ApiProperty({
    example: null,
    description: '삭제일시',
    nullable: true,
    required: false,
  })
  deletedAt: Date;

  @ApiProperty({
    example: null,
    description: '삭제자',
    nullable: true,
    required: true,
  })
  deletedBy: string;

  @ApiProperty({
    example: BooleanTypes.Y,
    description: '사용여부',
    enum: BooleanTypes,
    nullable: false,
    required: false,
  })
  useYn: BooleanTypes;

  @ApiProperty({
    example: '',
    description: '비고',
    nullable: true,
    required: false,
  })
  remark: string;
}
