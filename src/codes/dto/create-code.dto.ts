import { ApiProperty } from '@nestjs/swagger';
import { Prisma } from '@prisma/client';
import { IsNotEmpty, IsString } from 'class-validator';
import { CommonEntityBuilder } from 'src/common/dto/validator/common-entity.builder';
import { CommonValidateFunction } from 'src/common/dto/validator/common-validate.function';

export class CreateCodeDto
  implements CommonEntityBuilder<Prisma.CodeCreateArgs>
{
  codeGroup: string;
  createdBy: string;

  @ApiProperty({
    description: '코드',
    example: 'ADMIN',
  })
  @IsString()
  @IsNotEmpty()
  code: string;

  @ApiProperty({
    description: '코드설명',
    example: '관리자회원',
  })
  @IsString()
  @IsNotEmpty()
  codeNm: string;

  async build(): Promise<Prisma.CodeCreateArgs> {
    const model: Prisma.CodeCreateArgs = {
      data: {
        code: this.code,
        codeGroup: this.codeGroup,
        codeNm: this.codeNm,
        createdBy: this.createdBy,
        updatedBy: this.createdBy,
      },
      // select: {
      //   codeGroup: true,
      //   code: true,
      // },
    };
    return model;
  }
  async validate(validateFunction: CommonValidateFunction<any>): Promise<this> {
    await validateFunction.validate(this);
    return this;
  }
}
