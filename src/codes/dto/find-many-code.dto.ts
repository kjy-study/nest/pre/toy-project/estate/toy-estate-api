import { Prisma } from '@prisma/client';
import { CommonFilterDto } from '../../common/dto/common-filter.dto';
import { CommonEntityBuilder } from '../../common/dto/validator/common-entity.builder';
import { CommonValidateFunction } from '../../common/dto/validator/common-validate.function';
import { FindManyBuilderType } from '../../common/types/common.type';

export class FindManyCodeDto
  extends CommonFilterDto
  implements
    CommonEntityBuilder<
      FindManyBuilderType<Prisma.CodeFindManyArgs, Prisma.CodeCountArgs>
    >
{
  codeGroup: string;

  async build(): Promise<
    FindManyBuilderType<Prisma.CodeFindManyArgs, Prisma.CodeCountArgs>
  > {
    const findManyArgs: Prisma.CodeFindManyArgs = {
      where: {
        codeGroup: this.codeGroup,
      },
      skip: this.page * this.size,
      take: this.size,
    };

    const countArgs: Prisma.CodeCountArgs = Object.assign(findManyArgs);

    return {
      findManyArgs: findManyArgs,
      countManyArgs: countArgs,
    };
  }

  // 목록 조회는 유효성 검사는 하지 않습니다.
  async validate(validateFunction: CommonValidateFunction<any>): Promise<this> {
    return this;
  }
}
