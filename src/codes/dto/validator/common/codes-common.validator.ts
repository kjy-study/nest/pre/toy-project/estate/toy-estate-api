import { Injectable, NotFoundException } from '@nestjs/common';
import { CodesRepository } from '../../../codes.repository';
import { CodeExceptionMessage } from '../../../exception/codes.exception.message';

@Injectable()
export class CodesCommonValidator {
  constructor(protected readonly codesRepository: CodesRepository) {}

  /**
   * 코드 그룹이 존재하는지 유효성 검사
   * @param params
   */
  async validateCodeGroupExists(params: { codeGroup: string }) {
    const savedCodeGroup = await this.codesRepository.findFirst({
      where: {
        codeGroup: params.codeGroup,
      },
    });

    if (!savedCodeGroup) {
      throw new NotFoundException(
        CodeExceptionMessage.NotExistsCodeGroup(params.codeGroup),
      );
    }

    return savedCodeGroup;
  }

  /**
   * 코드가 존재하는지 유효성 검사
   * @param params
   * @returns
   */
  async validateCodeExists(params: { codeGroup: string; code: string }) {
    const savedCode = await this.codesRepository.findUnique({
      where: {
        codeGroup_code: {
          codeGroup: params.codeGroup,
          code: params.code,
        },
      },
    });

    if (!savedCode) {
      throw new NotFoundException(
        CodeExceptionMessage.NotExistsCode(params.codeGroup, params.code),
      );
    }

    return savedCode;
  }
}
