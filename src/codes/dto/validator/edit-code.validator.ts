import { Injectable } from '@nestjs/common';
import { CommonValidateFunction } from '../../../common/dto/validator/common-validate.function';
import { EditCodeDto } from '../edit-code.dto';
import { CodesCommonValidator } from './common/codes-common.validator';

@Injectable()
export class EditCodeValidator
  extends CodesCommonValidator
  implements CommonValidateFunction<EditCodeDto>
{
  async validate(dto: EditCodeDto): Promise<void> {
    await super.validateCodeGroupExists({
      codeGroup: dto.codeGroup,
    });

    await super.validateCodeExists({
      codeGroup: dto.codeGroup,
      code: dto.code,
    });
  }
}
