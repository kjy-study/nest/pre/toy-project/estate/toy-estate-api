import { Injectable } from '@nestjs/common';
import { CommonValidateFunction } from '../../../common/dto/validator/common-validate.function';
import { RemoveCodeDto } from '../remove-code.dto';
import { CodesCommonValidator } from './common/codes-common.validator';

@Injectable()
export class RemoveCodeValidator
  extends CodesCommonValidator
  implements CommonValidateFunction<RemoveCodeDto>
{
  async validate(dto: RemoveCodeDto): Promise<void> {
    await super.validateCodeExists({
      codeGroup: dto.codeGroup,
      code: dto.code,
    });
  }
}
