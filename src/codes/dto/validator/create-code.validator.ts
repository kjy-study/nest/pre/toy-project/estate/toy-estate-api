import { ConflictException, Injectable } from '@nestjs/common';
import { CommonValidateFunction } from '../../../common/dto/validator/common-validate.function';
import { CodeExceptionMessage } from '../../exception/codes.exception.message';
import { CreateCodeDto } from '../create-code.dto';
import { CodesCommonValidator } from './common/codes-common.validator';

@Injectable()
export class CreateCodeValidator
  extends CodesCommonValidator
  implements CommonValidateFunction<CreateCodeDto>
{
  async validate(dto: CreateCodeDto): Promise<void> {
    const savedCode = await this.codesRepository.findUnique({
      where: {
        codeGroup_code: {
          codeGroup: dto.codeGroup,
          code: dto.code,
        },
      },
    });

    if (savedCode) {
      throw new ConflictException(
        CodeExceptionMessage.DuplicateCode(dto.codeGroup, dto.code),
      );
    }
  }
}
