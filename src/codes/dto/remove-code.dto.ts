import { BooleanTypes, Prisma } from '@prisma/client';
import { CommonEntityBuilder } from '../../common/dto/validator/common-entity.builder';
import { CommonValidateFunction } from '../../common/dto/validator/common-validate.function';
import { dbNow } from '../../common/service/prisma.service';

export class RemoveCodeDto
  implements CommonEntityBuilder<Prisma.CodeUpdateArgs>
{
  codeGroup: string;
  code: string;
  deletedBy: string;

  useYn: BooleanTypes = BooleanTypes.Y;

  async build(): Promise<Prisma.CodeUpdateArgs> {
    const model: Prisma.CodeUpdateArgs = {
      data: {
        useYn: this.useYn,
        deletedBy: this.deletedBy,
        deletedAt: dbNow(),
      },
      where: {
        codeGroup_code: {
          code: this.code,
          codeGroup: this.codeGroup,
        },
      },
    };

    return model;
  }

  async validate(validateFunction: CommonValidateFunction<any>): Promise<this> {
    await validateFunction.validate(this);
    return this;
  }
}
