import { PickType } from '@nestjs/swagger';
import { Prisma } from '@prisma/client';
import { CommonEntityBuilder } from '../../common/dto/validator/common-entity.builder';
import { CommonValidateFunction } from '../../common/dto/validator/common-validate.function';
import { CreateCodeDto } from './create-code.dto';

export class EditCodeDto
  extends PickType(CreateCodeDto, ['codeNm'])
  implements CommonEntityBuilder<Prisma.CodeUpdateArgs>
{
  codeGroup: string;
  code: string;
  updatedBy: string;

  async build(params?: any): Promise<Prisma.CodeUpdateArgs> {
    return {
      data: {
        codeNm: this.codeNm,
        updatedBy: this.updatedBy,
      },
      where: {
        codeGroup_code: {
          codeGroup: this.codeGroup,
          code: this.code,
        },
      },
    };
  }

  async validate(
    validateFunction: CommonValidateFunction<any>,
    parmas?: any,
  ): Promise<this> {
    await validateFunction.validate(this);
    return this;
  }
}
