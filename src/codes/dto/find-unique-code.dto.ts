import { Prisma } from '@prisma/client';
import { CommonEntityBuilder } from '../../common/dto/validator/common-entity.builder';
import { CommonValidateFunction } from '../../common/dto/validator/common-validate.function';

export class FindUniqueCodeDto
  implements CommonEntityBuilder<Prisma.CodeFindUniqueArgs>
{
  codeGroup: string;
  code: string;

  async build(): Promise<Prisma.CodeFindUniqueArgs> {
    const model: Prisma.CodeFindUniqueArgs = {
      where: {
        codeGroup_code: {
          codeGroup: this.codeGroup,
          code: this.code,
        },
      },
    };
    return model;
  }
  async validate(validateFunction: CommonValidateFunction<any>): Promise<this> {
    return this;
  }
}
