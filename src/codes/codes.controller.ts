import {
  Body,
  Controller,
  Delete,
  Get,
  Header,
  Param,
  Patch,
  Post,
  Query,
  Version,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiExtraModels,
  ApiParam,
  ApiTags,
  getSchemaPath,
} from '@nestjs/swagger';
import { JwtPayload } from '../auth/auth.service';
import { Public } from '../auth/meta/public.meta';
import { Roles } from '../auth/meta/roles.meta';
import { RoleTypes } from '../common/constraint/common.const';
import { JwtUser } from '../common/decorator/jwt-user.param-decorator';
import { RestParam } from '../common/decorator/rest.param-decorator';
import {
  ApiOperationFormat,
  ApiOperationMethodTypes,
} from '../common/decorator/swagger-operation.decorator';
import {
  ApiCompositeCreateResponse,
  ApiCompositeFindAllResponse,
  ApiCompositeFindOneResponse,
} from '../common/decorator/swagger-response.decorator';
import {
  CreateResponseForm,
  EditResponseForm,
  InfoResponseForm,
  ListResponseForm,
  RemoveResponseForm,
} from '../common/response/response.form';
import { CodesService } from './codes.service';
import { CreateCodeDto } from './dto/create-code.dto';
import { EditCodeDto } from './dto/edit-code.dto';
import { FindManyCodeDto } from './dto/find-many-code.dto';
import { FindUniqueCodeDto } from './dto/find-unique-code.dto';
import { RemoveCodeDto } from './dto/remove-code.dto';
import { CodeEntity } from './entities/code.entity';

@ApiTags('[관리자] 코드 관리')
@ApiBearerAuth('access-token')
@Controller({ path: 'codeGroup/:codeGroup/codes', version: '1' })
@ApiExtraModels(
  CodeEntity,
  ListResponseForm,
  InfoResponseForm,
  CreateResponseForm,
  EditResponseForm,
  RemoveResponseForm,
)
export class CodesController {//
  constructor(private readonly codeService: CodesService) {}

  //
  @Post()
  @Roles(RoleTypes.ADMIN, RoleTypes.SYSTEM)
  @Header('content-type', 'application/json')
  @ApiParam({
    name: 'codeGroup',
    example: 'TEST',
    description: '코드 그룹',
  })
  @ApiOperationFormat('코드', ApiOperationMethodTypes.CREATE)
  @ApiCompositeCreateResponse({ type: CreateResponseForm })
  @Version('1')
  async create(
    @RestParam('codeGroup') codeGroup: string,
    @JwtUser() user: JwtPayload,
    @Body() dto: CreateCodeDto,
  ) {
    dto.createdBy = user.email;
    dto.codeGroup = codeGroup;
    const savedData = await this.codeService.create({
      dto,
    });
    return {
      ...new CreateResponseForm(),
      codeGroup: savedData.codeGroup,
      code: savedData.code,
    };
  }
  //
  @Get()
  @Public()
  @Header('content-type', 'application/json')
  @ApiParam({
    name: 'codeGroup',
    example: 'TEST',
    description: '코드 그룹',
  })
  @ApiOperationFormat('코드', ApiOperationMethodTypes.FIND_ALL)
  @ApiCompositeFindAllResponse({ type: ListResponseForm })
  async findMany(
    @Query() dto: FindManyCodeDto,
    @RestParam('codeGroup') codeGroup: string,
  ) {
    dto.codeGroup = codeGroup;

    const { datas, count } = await this.codeService.findMany({ dto });
    return new ListResponseForm(dto, count, datas);
  }

  @Get(':code')
  @Header('content-type', 'application/json')
  @ApiOperationFormat('코드', ApiOperationMethodTypes.FIND_ONE)
  @ApiCompositeFindOneResponse({
    schema: {
      allOf: [
        { $ref: getSchemaPath(InfoResponseForm) },
        {
          properties: {
            item: {
              $ref: getSchemaPath(CodeEntity),
            },
          },
        },
      ],
    },
  })
  async findUnique(
    @RestParam('codeGroup') codeGroup: string,
    @RestParam('code') code: string,
  ) {
    const dto = new FindUniqueCodeDto();
    dto.codeGroup = codeGroup;
    dto.code = code;

    const savedData = await this.codeService.findUnique({
      dto: dto,
    });

    return new InfoResponseForm(savedData);
  }

  @Patch(':code')
  @Roles(RoleTypes.ADMIN, RoleTypes.SYSTEM)
  @Header('content-type', 'application/json')
  @ApiParam({
    name: 'codeGroup',
    example: 'TEST',
    description: '코드 그룹',
  })
  @ApiOperationFormat('코드', ApiOperationMethodTypes.EDIT)
  @ApiCompositeCreateResponse({
    schema: {
      allOf: [
        {
          $ref: getSchemaPath(EditResponseForm),
        },
        {
          properties: {
            editedId: {
              deprecated: true,
            },
          },
        },
        {
          properties: {
            codeGroup: {
              type: 'string',
              example: 'TEST',
            },
          },
        },
        {
          properties: {
            code: {
              type: 'string',
              example: 'TEST-001',
            },
          },
        },
      ],
    },
  })
  @Version('1')
  async edit(
    @Param('codeGroup') codeGroup: string,
    @Param('code') code: string,
    @JwtUser() user: JwtPayload,
    @Body() dto: EditCodeDto,
  ) {
    dto.codeGroup = codeGroup;
    dto.code = code;
    dto.updatedBy = user.email;
    const editedData = await this.codeService.edit({ dto });
    return {
      ...new EditResponseForm(),
      codeGroup: editedData.codeGroup,
      code: editedData.code,
    };
  }

  @Delete(':code')
  @Roles(RoleTypes.ADMIN, RoleTypes.SYSTEM)
  @Header('content-type', 'application/json')
  @ApiParam({
    name: 'codeGroup',
    example: 'TEST',
    description: '코드 그룹',
  })
  @ApiParam({
    name: 'code',
    example: 'TEST-001',
    description: '코드',
  })
  @ApiOperationFormat('코드', ApiOperationMethodTypes.REMOVE)
  @ApiCompositeCreateResponse({
    schema: {
      allOf: [
        {
          $ref: getSchemaPath(RemoveResponseForm),
        },
        {
          properties: {
            removedId: {
              deprecated: true,
            },
          },
        },
        {
          properties: {
            codeGroup: {
              type: 'string',
              example: 'TEST',
            },
          },
        },
        {
          properties: {
            code: {
              type: 'string',
              example: 'TEST-001',
            },
          },
        },
      ],
    },
  })
  async remove(
    @RestParam('codeGroup') codeGroup: string,
    @RestParam('code') code: string,
    @JwtUser() user: JwtPayload,
  ) {
    const dto = new RemoveCodeDto();
    dto.codeGroup = codeGroup;
    dto.code = code;

    dto.deletedBy = user.email;

    const removedData = await this.codeService.remove({
      dto,
    });

    return { ...new RemoveResponseForm(), codeGroup, code };
  }
}
