/*
  Warnings:

  - You are about to drop the `useronrole` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE `useronrole` DROP FOREIGN KEY `UserOnRole_role_id_fkey`;

-- DropForeignKey
ALTER TABLE `useronrole` DROP FOREIGN KEY `UserOnRole_user_id_fkey`;

-- DropTable
DROP TABLE `useronrole`;

-- CreateTable
CREATE TABLE `user_on_role` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `role_id` VARCHAR(191) NOT NULL,
    `user_id` INTEGER NOT NULL,
    `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `created_by` VARCHAR(191) NOT NULL,
    `updated_at` DATETIME(3) NOT NULL,
    `updated_by` VARCHAR(191) NOT NULL,
    `deleted_at` DATETIME(3) NULL,
    `deleted_by` VARCHAR(191) NULL,
    `use_yn` ENUM('Y', 'N') NOT NULL DEFAULT 'Y',
    `remark` VARCHAR(191) NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `user_on_role` ADD CONSTRAINT `user_on_role_user_id_fkey` FOREIGN KEY (`user_id`) REFERENCES `User`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `user_on_role` ADD CONSTRAINT `user_on_role_role_id_fkey` FOREIGN KEY (`role_id`) REFERENCES `Role`(`role`) ON DELETE CASCADE ON UPDATE CASCADE;
