/*
  Warnings:

  - The primary key for the `permission` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `id` on the `permission` table. All the data in the column will be lost.
  - Added the required column `behavior` to the `Permission` table without a default value. This is not possible if the table is not empty.
  - Added the required column `service` to the `Permission` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `permission` DROP PRIMARY KEY,
    DROP COLUMN `id`,
    ADD COLUMN `behavior` VARCHAR(191) NOT NULL,
    ADD COLUMN `service` VARCHAR(191) NOT NULL,
    ADD PRIMARY KEY (`service`, `behavior`);
