-- DropForeignKey
ALTER TABLE `post` DROP FOREIGN KEY `Post_author_id_fkey`;

-- DropForeignKey
ALTER TABLE `profile` DROP FOREIGN KEY `Profile_user_id_fkey`;

-- AlterTable
ALTER TABLE `code` MODIFY `deleted_at` DATETIME(3) NULL;

-- AlterTable
ALTER TABLE `post` MODIFY `deleted_at` DATETIME(3) NULL;

-- AlterTable
ALTER TABLE `profile` MODIFY `deleted_at` DATETIME(3) NULL;

-- AlterTable
ALTER TABLE `user` MODIFY `deleted_at` DATETIME(3) NULL;

-- AddForeignKey
ALTER TABLE `Post` ADD CONSTRAINT `Post_author_id_fkey` FOREIGN KEY (`author_id`) REFERENCES `User`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Profile` ADD CONSTRAINT `Profile_user_id_fkey` FOREIGN KEY (`user_id`) REFERENCES `User`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
