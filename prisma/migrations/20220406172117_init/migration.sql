/*
  Warnings:

  - You are about to drop the column `authorId` on the `post` table. All the data in the column will be lost.
  - You are about to drop the column `userId` on the `profile` table. All the data in the column will be lost.
  - A unique constraint covering the columns `[user_id]` on the table `Profile` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `author_id` to the `Post` table without a default value. This is not possible if the table is not empty.
  - Added the required column `created_by` to the `Post` table without a default value. This is not possible if the table is not empty.
  - Added the required column `deleted_at` to the `Post` table without a default value. This is not possible if the table is not empty.
  - Added the required column `updated_by` to the `Post` table without a default value. This is not possible if the table is not empty.
  - Added the required column `created_by` to the `Profile` table without a default value. This is not possible if the table is not empty.
  - Added the required column `deleted_at` to the `Profile` table without a default value. This is not possible if the table is not empty.
  - Added the required column `updated_at` to the `Profile` table without a default value. This is not possible if the table is not empty.
  - Added the required column `updated_by` to the `Profile` table without a default value. This is not possible if the table is not empty.
  - Added the required column `user_id` to the `Profile` table without a default value. This is not possible if the table is not empty.
  - Added the required column `created_by` to the `User` table without a default value. This is not possible if the table is not empty.
  - Added the required column `deleted_at` to the `User` table without a default value. This is not possible if the table is not empty.
  - Added the required column `updated_at` to the `User` table without a default value. This is not possible if the table is not empty.
  - Added the required column `updated_by` to the `User` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE `post` DROP FOREIGN KEY `Post_authorId_fkey`;

-- DropForeignKey
ALTER TABLE `profile` DROP FOREIGN KEY `Profile_userId_fkey`;

-- AlterTable
ALTER TABLE `post` DROP COLUMN `authorId`,
    ADD COLUMN `author_id` INTEGER NOT NULL,
    ADD COLUMN `created_by` VARCHAR(191) NOT NULL,
    ADD COLUMN `deleted_at` DATETIME(3) NOT NULL,
    ADD COLUMN `deleted_by` VARCHAR(191) NULL,
    ADD COLUMN `updated_by` VARCHAR(191) NOT NULL,
    ADD COLUMN `use_yn` ENUM('Y', 'N') NOT NULL DEFAULT 'Y';

-- AlterTable
ALTER TABLE `profile` DROP COLUMN `userId`,
    ADD COLUMN `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    ADD COLUMN `created_by` VARCHAR(191) NOT NULL,
    ADD COLUMN `deleted_at` DATETIME(3) NOT NULL,
    ADD COLUMN `deleted_by` VARCHAR(191) NULL,
    ADD COLUMN `updated_at` DATETIME(3) NOT NULL,
    ADD COLUMN `updated_by` VARCHAR(191) NOT NULL,
    ADD COLUMN `use_yn` ENUM('Y', 'N') NOT NULL DEFAULT 'Y',
    ADD COLUMN `user_id` INTEGER NOT NULL;

-- AlterTable
ALTER TABLE `user` ADD COLUMN `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    ADD COLUMN `created_by` VARCHAR(191) NOT NULL,
    ADD COLUMN `deleted_at` DATETIME(3) NOT NULL,
    ADD COLUMN `deleted_by` VARCHAR(191) NULL,
    ADD COLUMN `updated_at` DATETIME(3) NOT NULL,
    ADD COLUMN `updated_by` VARCHAR(191) NOT NULL,
    ADD COLUMN `use_yn` ENUM('Y', 'N') NOT NULL DEFAULT 'Y';

-- CreateTable
CREATE TABLE `Code` (
    `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `created_by` VARCHAR(191) NOT NULL,
    `updated_at` DATETIME(3) NOT NULL,
    `updated_by` VARCHAR(191) NOT NULL,
    `deleted_at` DATETIME(3) NOT NULL,
    `deleted_by` VARCHAR(191) NULL,
    `use_yn` ENUM('Y', 'N') NOT NULL DEFAULT 'Y',
    `code` VARCHAR(100) NOT NULL,
    `code_group` VARCHAR(100) NOT NULL,
    `code_nm` VARCHAR(100) NULL,

    PRIMARY KEY (`code_group`, `code`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateIndex
CREATE UNIQUE INDEX `Profile_user_id_key` ON `Profile`(`user_id`);

-- AddForeignKey
ALTER TABLE `Post` ADD CONSTRAINT `Post_author_id_fkey` FOREIGN KEY (`author_id`) REFERENCES `User`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Profile` ADD CONSTRAINT `Profile_user_id_fkey` FOREIGN KEY (`user_id`) REFERENCES `User`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
