import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { Code } from '@prisma/client';
import request from 'supertest';
import { AppModule } from '../../src/app.module';
import { CreateCodeDto } from '../../src/codes/dto/create-code.dto';
import { EditCodeDto } from '../../src/codes/dto/edit-code.dto';
import { CodeExceptionMessage } from '../../src/codes/exception/codes.exception.message';
import { PrismaService } from '../../src/common/service/prisma.service';
import { setGlobalOption } from '../../src/main';
import { EditUserDto } from '../../src/users/dto/edit-user.dto';

jest.setTimeout(10 * 60 * 1000); // 디버깅 시 timeOut이 발생되어 추가했습니다.(default: 5000ms)

describe('CodeController (e2e)', () => {
  let app: INestApplication;
  let accessToken: string;
  let prisma: PrismaService;
  const savedCodes: Code[] = [];

  beforeEach(async () => {
    // e2e 테스트를 위해 AppModule를 불러와 의존성 주입 합니다.
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();

    await setGlobalOption(app);

    prisma = moduleFixture.get<PrismaService>(PrismaService);

    await app.init();
    // await app.listen(80);
  });

  afterEach(async () => {
    await app.close();
  });

  afterAll(async () => {
    for (const savedCode of savedCodes) {
      await prisma.code.delete({
        where: {
          codeGroup_code: {
            codeGroup: savedCode.codeGroup,
            code: savedCode.code,
          },
        },
      });
    }
  });

  it('토큰 발급 (e2e)', async () => {
    const data = await request(app.getHttpServer())
      .post('/v1/auth/token')
      .send({
        email: 'mion@gmail.com',
        password: 'password1234!',
      })
      .expect(201);

    expect(data.body).toHaveProperty(['accessToken']);
    expect(data.body).toHaveProperty(['refreshToken']);

    accessToken = data.body.accessToken;

    return data;
  });

  describe('코드 등록 (e2e)', () => {
    const reqCreateBody = new CreateCodeDto();
    reqCreateBody.code = 'e2e_test_code';
    reqCreateBody.codeNm = '테스트입니다.';

    const reqEditBody = new EditUserDto();
    reqEditBody.name = 'editedMion';

    it('코드 등록', async () => {
      const codeGroup = 'e2e_test';
      const data = await request(app.getHttpServer())
        .post(`/v1/codeGroup/${codeGroup}/codes`)
        .set('Authorization', `Bearer ${accessToken}`)
        // .set('uid', uid)
        .send(reqCreateBody)
        .expect(201);

      const savedCode = await prisma.code.findUnique({
        where: {
          codeGroup_code: {
            codeGroup: codeGroup,
            code: reqCreateBody.code,
          },
        },
      });
      savedCodes.push(savedCode);
      return data;
    });

    it('이미 등록되어있는 코드 등록 시 에러 반환', async () => {
      const codeGroup = 'e2e_test';
      const { body } = await request(app.getHttpServer())
        .post(`/v1/codeGroup/${codeGroup}/codes`)
        .set('Authorization', `Bearer ${accessToken}`)
        // .set('uid', uid)
        .send(reqCreateBody)
        .expect(409);

      expect(body.message).toEqual(
        CodeExceptionMessage.DuplicateCode(codeGroup, reqCreateBody.code),
      );

      return body;
    });
  });

  describe('코드 수정 (e2e)', () => {
    const reqEditBody = new EditCodeDto();
    reqEditBody.codeNm = '수정된 코드 이름입니다.';

    it('코드 그룹이 없는 데이터 수정 시 오류 반환', async () => {
      const codeGroup = 'unkown';
      const code = 'unkown';

      const { body } = await request(app.getHttpServer())
        .patch(`/v1/codeGroup/${codeGroup}/codes/${code}`)
        .set('Authorization', `Bearer ${accessToken}`)
        .send(reqEditBody)
        .expect(404);

      expect(body.message).toEqual(
        CodeExceptionMessage.NotExistsCodeGroup(codeGroup),
      );
    });
    it('코드가 없는 데이터 수정 시 오류 반환', async () => {
      const codeGroup = savedCodes[0].codeGroup;
      const code = 'unkown';
      const { body } = await request(app.getHttpServer())
        .patch(`/v1/codeGroup/${codeGroup}/codes/${code}`)
        .set('Authorization', `Bearer ${accessToken}`)
        .send(reqEditBody)
        .expect(404);

      expect(body.message).toEqual(
        CodeExceptionMessage.NotExistsCode(codeGroup, code),
      );
    });
    it('정상 수정', async () => {
      const codeGroup = savedCodes[0].codeGroup;
      const code = savedCodes[0].code;
      const { body } = await request(app.getHttpServer())
        .patch(`/v1/codeGroup/${codeGroup}/codes/${code}`)
        .set('Authorization', `Bearer ${accessToken}`)
        .send(reqEditBody)
        .expect(200);

      expect(body.message).toEqual('정상적으로 수정되었습니다.');
    });
  });
});
