import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { User } from '@prisma/client';
import request from 'supertest';
import { AppModule } from '../../src/app.module';
import { PrismaService } from '../../src/common/service/prisma.service';
import { setGlobalOption } from '../../src/main';
import { CreateUserDto } from '../../src/users/dto/create-user.dto';
import { EditUserDto } from '../../src/users/dto/edit-user.dto';
import { UserExceptionMessage } from '../../src/users/exceptions/users.exception.message';

jest.setTimeout(10 * 60 * 1000); // 디버깅 시 timeOut이 발생되어 추가했습니다.(default: 5000ms)

describe('UserController (e2e)', () => {
  let app: INestApplication;
  let accessToken: string;
  let prisma: PrismaService;
  const savedUsers: User[] = [];

  beforeEach(async () => {
    // e2e 테스트를 위해 AppModule를 불러와 의존성 주입 합니다.
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();

    await setGlobalOption(app);

    prisma = moduleFixture.get<PrismaService>(PrismaService);

    await app.init();
    // await app.listen(80);
  });

  afterEach(async () => {
    await app.close();
  });

  afterAll(async () => {
    for (const savedUser of savedUsers) {
      await prisma.user.delete({
        where: {
          id: savedUser.id,
        },
      });
    }
  });

  it('토큰 발급 (e2e)', async () => {
    const data = await request(app.getHttpServer())
      .post('/v1/auth/token')
      .send({
        email: 'mion@gmail.com',
        password: 'password1234!',
      })
      .expect(201);

    expect(data.body).toHaveProperty(['accessToken']);
    expect(data.body).toHaveProperty(['refreshToken']);

    accessToken = data.body.accessToken;

    return data;
  });

  describe('사용자 등록 (e2e)', () => {
    const reqCreateBody = new CreateUserDto();
    reqCreateBody.email = `${Math.random()}mion@gmail.com`;
    reqCreateBody.name = 'mion';
    reqCreateBody.password = 'password1234!';

    it('비밀번호 정규식이 다를경우(특수문자 없을 때)', async () => {
      const data = await request(app.getHttpServer())
        .post(`/v1/users`)
        .set('Authorization', `Bearer ${accessToken}`)
        .send({ ...reqCreateBody, password: 'password1234' })
        .expect(400);

      expect(data.body.message?.[0]).toEqual(
        UserExceptionMessage.InvalidPasswordForm(),
      );

      return data;
    });

    it('비밀번호 정규식이 다를경우(숫자 없을 때)', async () => {
      const data = await request(app.getHttpServer())
        .post(`/v1/users`)
        .set('Authorization', `Bearer ${accessToken}`)
        .send({ ...reqCreateBody, password: 'password!@#$' })
        .expect(400);

      expect(data.body.message?.[0]).toEqual(
        UserExceptionMessage.InvalidPasswordForm(),
      );

      return data;
    });

    it('비밀번호 정규식이 다를경우(8자리 안될 때)', async () => {
      const data = await request(app.getHttpServer())
        .post(`/v1/users`)
        .set('Authorization', `Bearer ${accessToken}`)
        .send({ ...reqCreateBody, password: 'pa@#$' })
        .expect(400);

      expect(data.body.message?.[0]).toEqual(
        UserExceptionMessage.InvalidPasswordForm(),
      );

      return data;
    });

    it('사용자 등록', async () => {
      const data = await request(app.getHttpServer())
        .post(`/v1/users`)
        .set('Authorization', `Bearer ${accessToken}`)
        .send(reqCreateBody)
        .expect(201);

      const savedUser = await prisma.user.findUnique({
        where: {
          id: data.body.createdId,
        },
      });
      savedUsers.push(savedUser);
      return data;
    });

    it('중복된 이메일로 사용자 가입 시 에러 반환', async () => {
      const { body } = await request(app.getHttpServer())
        .post(`/v1/users`)
        .set('Authorization', `Bearer ${accessToken}`)
        .send(reqCreateBody)
        .expect(400);

      expect(body.message).toEqual(
        UserExceptionMessage.DuplicateEmail(reqCreateBody.email),
      );

      return body;
    });
  });
  describe('사용자 수정 (e2e)', () => {
    const reqEditBody = new EditUserDto();
    reqEditBody.name = 'editedMion';

    it('존재하지 않는 사용자 정보 수정 시 에러 반환', async () => {
      const userId = 12312323123;
      const data = await request(app.getHttpServer())
        .patch(`/v1/users/${userId}`)
        .set('Authorization', `Bearer ${accessToken}`)
        .send(reqEditBody)
        .expect(404);

      expect(data.body.message).toEqual(
        UserExceptionMessage.NotExistsUser(userId),
      );
    });

    it('사용자 정보 수정', async () => {
      const data = await request(app.getHttpServer())
        .patch(`/v1/users/${savedUsers[0].id}`)
        .set('Authorization', `Bearer ${accessToken}`)
        .send(reqEditBody)
        .expect(200);

      const editedUser = await prisma.user.findUnique({
        where: {
          id: data.body.editedId,
        },
      });

      expect(editedUser.name).toEqual(reqEditBody.name);
    });
  });
});
